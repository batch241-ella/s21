console.log("Hello World!");

let users = ["Dwayne Johnson", "Steve Austin", "Kurt Angle", "Dave Bautista"];
console.log("Original Array:");
console.log(users);

function addToArray(data) {
	users[users.length] = data;
}

addToArray("John Cena");
console.log(users);

function getArrayItem(index) {
	return users[index];
}

let itemFound = getArrayItem(2);
console.log(itemFound);

function deleteLastArrayItem() {
	let lastItem = users[users.length - 1];
	users.length--;

	return lastItem;
}

let deletedItem = deleteLastArrayItem();
console.log(deletedItem);
console.log(users);

function updateArray(newVal, index) {
	users[index] = newVal;
}

updateArray("Triple H", 3);
console.log(users);

function deleteAllArrayItems() {
	users.length = 0;
}

deleteAllArrayItems();
console.log(users);

function isArrEmpty() {
	if (users.length > 0) {
		return false;
	} else {
		return true;
	}
}

let isUsersEmpty = isArrEmpty();
console.log(isUsersEmpty);